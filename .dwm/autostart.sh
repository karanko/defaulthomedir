#! /bin/bash
slstatus &
dbus-launch &
#feh --randomize --bg-fill "$(getent passwd "1000" | cut -d: -f6  )/Pictures/NASA"

 #Define shader
GRAYSCALE=$(cat <<-END
uniform sampler2D tex;
void main() {
   vec4 c = texture2D(tex, gl_TexCoord[0].xy);
   float y = dot(c.rgb, vec3(0.299, 0.587, 0.114));
   vec4 gray = vec4(y, y, y, 1.0);
   gl_FragColor = mix(c, gray, 0.95);
}
END
)

#compton -o .75 -c  -e 0.00001 -m 0.75 --glx-fshader-win "$GRAYSCALE" --backend glx  &
#compton -o .75 -c  -e 0.00001 -m 0.75  &

#mkdir -p  "$(getent passwd "1000" | cut -d: -f6  )/Pictures/NASA"
#cd "$(getent passwd "1000" | cut -d: -f6  )/Pictures/NASA"

#wget `curl -s "https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY&count=10" | jq '.[].hdurl' | tr -d '"'` -nc &
#xrandr --dpi 86
#xrandr --setprovideroutputsource 4 0
#xrandr --output DVI-I-1-1 --auto --left-of eDP-1
